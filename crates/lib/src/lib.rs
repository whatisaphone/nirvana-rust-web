#![warn(future_incompatible, rust_2018_compatibility, rust_2018_idioms, unused)]
#![warn(clippy::pedantic)]
// #![warn(clippy::cargo)]
#![cfg_attr(feature = "strict", deny(warnings))]

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
#[must_use]
pub fn add(x: i32, y: i32) -> i32 {
    x + y
}

#[cfg(test)]
mod tests {
    use crate::add;

    #[test]
    fn it_works() {
        assert_eq!(add(2, 2), 4);
    }
}

#[cfg(test)]
mod wasm_bindgen_tests {
    use crate::add;
    use wasm_bindgen::prelude::*;
    use wasm_bindgen_test::{wasm_bindgen_test, wasm_bindgen_test_configure};

    wasm_bindgen_test_configure!(run_in_browser);

    #[wasm_bindgen_test]
    fn it_works() {
        let two = JsValue::from("2").as_string().unwrap().parse().unwrap();
        assert_eq!(add(two, two), 4);
    }
}
