// https://stylelint.io/user-guide/configure

const nirvana = require('nirvana-stylelint');

const config = nirvana.base();
module.exports = config;
