// https://eslint.org/docs/user-guide/configuring

const nirvana = require('nirvana-eslint');

const config = nirvana.base();
nirvana.addTypeScript(config);
nirvana.addJest(config);

module.exports = config;
