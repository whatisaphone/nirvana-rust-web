import { promises as fs } from 'fs';
import path from 'path';

import { WasmModule } from '../wasm';

// eslint-disable-next-line functional/prefer-readonly-type
export const instances: { [filename: string]: unknown } = {};

export async function loadWasm(): Promise<WasmModule> {
  // `require` is sync, and wasm instantiation is async, so we need to preload
  // the wasm before calling `require`.
  const filename = path.resolve(
    __dirname,
    '../../../../crates/lib/pkg/index_bg.wasm',
  );
  const buffer = await fs.readFile(filename);
  const instance = await WebAssembly.instantiate(buffer);
  instances[filename] = instance.instance.exports;

  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  return require('../../../../crates/lib/pkg');
}
