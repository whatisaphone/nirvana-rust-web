import { instances } from './wasm-loader';

[module.exports] = Object.values(instances);
