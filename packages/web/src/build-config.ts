// eslint-disable-next-line import/no-unused-modules
export interface BuildConfig {
  readonly dev: boolean;
}

declare global {
  const BUILD_CONFIG: BuildConfig;
}
