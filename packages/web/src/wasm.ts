export type WasmModule = typeof import('../../../crates/lib/pkg');

export function importWasm(): Promise<WasmModule> {
  return import('../../../crates/lib/pkg');
}
