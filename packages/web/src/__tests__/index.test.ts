import { loadWasm } from '../__fixtures__/wasm-loader';
import { WasmModule } from '../wasm';

describe('wasm', () => {
  let wasm: WasmModule;

  beforeAll(async () => {
    wasm = await loadWasm();
  });

  it('works', () => {
    expect(wasm.add(2, 2)).toBe(4);
  });
});
