import '../styles/index.scss';

import { importWasm } from './wasm';

main().catch((error) => {
  // eslint-disable-next-line no-console
  console.error(error);
  document.body.textContent = 'Error loading app';
});

async function main() {
  const { add } = await importWasm();
  const sum = add(2, 2);
  document.body.textContent = `2 + 2 = ${sum}`;
}
