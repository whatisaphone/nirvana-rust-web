const WasmPackPlugin = require('@wasm-tool/wasm-pack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = function webpackConfig(env) {
  const dev = !(env && env.prod);

  return {
    bail: true,
    mode: dev ? 'development' : 'production',
    entry: './src',
    resolve: {
      extensions: ['.js', '.ts', '.tsx'],
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: {
            loader: 'ts-loader',
            options: {
              transpileOnly: true, // Use `ForkTsCheckerWebpackPlugin` instead
            },
          },
        },
        {
          test: /\.scss$/,
          use: [
            ...(dev ? ['style-loader'] : [MiniCssExtractPlugin.loader]),
            'css-loader',
            'sass-loader',
          ],
        },
      ],
    },
    plugins: [
      new WasmPackPlugin({
        crateDirectory: path.resolve('../../crates/lib'),
        outDir: path.resolve('../../crates/lib/pkg'),
      }),
      new webpack.DefinePlugin({
        BUILD_CONFIG: JSON.stringify({ dev }),
      }),
      new ForkTsCheckerWebpackPlugin(),
      new HtmlWebpackPlugin({
        template: 'src/index.html',
      }),
      ...(dev ? [] : [new MiniCssExtractPlugin()]),
    ],
    experiments: {
      asyncWebAssembly: true,
    },
    devServer: {
      hot: true,
    },
  };
};
