// https://eslint.org/docs/user-guide/configuring

module.exports = {
  ignorePatterns: ['/coverage/', '/dist/'],
  overrides: [
    {
      files: ['./*.js', '**/{__fixtures__,__tests__}/**'],
      env: {
        node: true,
      },
    },
  ],
};
